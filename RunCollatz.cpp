// --------------
// RunCollatz.c++
// --------------

// --------
// includes
// --------

#include <iostream> // cin, cout

#include "Collatz.hpp"

using namespace std;

// ----
// main
// ----

int main () {
    collatz_solve(cin, cout);
    // create_test();
    return 0;}
