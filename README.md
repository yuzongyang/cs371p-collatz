# CS371p: Object-Oriented Programming Collatz Repo

* Name: Kelvin Yu  

* EID: kzy67    

* GitLab ID: yuzongyang

* HackerRank ID: yuzongyang

* Git SHA: feec15f203eb0ffc3e909f989b6e5232386ecb3c

* GitLab Pipelines: https://gitlab.com/yuzongyang/cs371p-collatz/-/pipelines

* Estimated completion time: 3 hrs 

* Actual completion time: 5 hrs

* Comments: Used a lazy cache as opposed to a meta cache as I used that in CS373
