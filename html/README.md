# CS371p: Object-Oriented Programming Collatz Repo

* Name: Kelvin Yu  

* EID: kzy67    

* GitLab ID: yuzongyang

* HackerRank ID: yuzongyang

* Git SHA: 3fd183c16060991381ac7973a3967192342bc72c

* GitLab Pipelines: https://gitlab.com/yuzongyang/cs371p-collatz/-/pipelines

* Estimated completion time: 3 hrs 

* Actual completion time: 5 hrs

* Comments: Used a lazy cache as opposed to a meta cache as I used that in CS373
