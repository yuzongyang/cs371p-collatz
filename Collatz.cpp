// -----------
// Collatz.c++
// -----------

// --------
// includes
// --------

#include <cassert>   // assert
#include <iostream>  // endl, istream, ostream
#include <sstream>   // istringstream
#include <string>    // getline, string
#include <tuple>     // make_tuple, tie, tuple
#include <utility>   // make_pair, pair
#include <cmath>     // max
#include <algorithm> // max
#include <vector>    // vector

#include "Collatz.hpp"

using namespace std;

// ------------
// collatz_read
// ------------

pair<int, int> collatz_read (const string& s) {
    istringstream sin(s);
    int i;
    int j;
    sin >> i >> j;
    return make_pair(i, j);
}

// ------------
// collatz_eval
// ------------

int lazyCache[1000000] = {0, 1};

int collatz_basic (const int index) {
    // <your code>
    int cycle_length = 1;
    long temp_index = index;
    // loop and store each index that has been solved in the lazy cache
    while (temp_index != 1) {
        if (temp_index < 1000000 && lazyCache[temp_index] != 0) {
            lazyCache[index] = cycle_length + lazyCache[temp_index] - 1;
            return lazyCache[index];
        }
        ++cycle_length;
        if (temp_index % 2 == 0) {
            temp_index /= 2;
        }
        else {
            temp_index = 3 * temp_index + 1;
        }
    }
    lazyCache[index] = cycle_length;
    return cycle_length;
}

tuple<int, int, int> collatz_eval (const pair<int, int>& p) {
    int start;
    int end;
    tie(start, end) = p;
    // <your code>

    // added assertions
    assert(start >= 1);
    assert(end >= 1);
    assert(start <= 1000000);
    assert(end <= 1000000);
    // if start is greater than end, then swap
    if (start > end) {
        swap(start, end);
    }

    // if (end + 1) / 2 is greater than start, then we can reduce the range
    if ((end + 1) / 2 > start) {
        start = (end + 1) / 2;
    }

    // loop from start to finish
    int maximum = 1;
    int cycle_length = 0;
    for (int i = start; i <= end; ++i) {
        cycle_length = 0;
        // if there is no cache hit then just run collatz_basic and store that into the cache
        if (lazyCache[i] == 0) {
            cycle_length = collatz_basic(i);
        }
        else {
            cycle_length = lazyCache[i];
        }
        // find the max cycle length
        if (cycle_length > maximum) {
            maximum = cycle_length;
        }
    }

    return make_tuple(p.first, p.second, maximum);
}

// -------------
// collatz_print
// -------------

void collatz_print (ostream& sout, const tuple<int, int, int>& t) {
    int i;
    int j;
    int v;
    tie(i, j, v) = t;
    sout << i << " " << j << " " << v << endl;
}

// -------------
// collatz_solve
// -------------

void collatz_solve (istream& sin, ostream& sout) {
    string s;
    while (getline(sin, s))
        collatz_print(sout, collatz_eval(collatz_read(s)));
}

// -----------
// create_test
// -----------

void create_test () {
    for (int i = 0; i < 500; ++i) {
        int r1 = rand() % 1000000 + 1;
        int r2 = rand() % 1000000 + 1;
        cout << r1 << " " << r2 << endl;
    }
}